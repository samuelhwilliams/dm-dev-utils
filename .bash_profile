source /usr/local/git/contrib/completion/git-prompt.sh
source /usr/local/git/contrib/completion/git-completion.bash


export PS1='\u:\w\[\033[01;35m\]$(__git_ps1 " [%s]")\[\033[00m\]$(__secret_ps1 " [%s]")$ '
export PATH=~/Library/Python/2.7/bin:$PATH
export NIX_PATH=:nixpkgs=/Users/samuelwilliams/.nix-defexpr/channels/nixos-17.03
export DM_CREDENTIALS_REPO=/Users/samuelwilliams/git/digitalmarketplace-credentials

if [ -f $(brew --prefix)/etc/bash_completion ]; then
  . $(brew --prefix)/etc/bash_completion
fi

export LSCOLORS=ExFxBxDxCxegedabagacad
alias ls='ls -GFh'

alias grep='grep --color=auto'
alias cfl='cf login -a api.cloud.service.gov.uk -u Samuel.Williams@digital.cabinet-office.gov.uk'

eval "$(direnv hook bash)"
eval "$(rbenv init -)"

source ~/.dm-dev-utils/dm-secret-cli
source ~/.dm-dev-utils/python-auto-venv
source ~/.dm-dev-utils/git-aliases.sh

if [ -e /Users/samuelwilliams/.nix-profile/etc/profile.d/nix.sh ]; then . /Users/samuelwilliams/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

test -e "${HOME}/.iterm2_shell_integration.bash" && source "${HOME}/.iterm2_shell_integration.bash"
