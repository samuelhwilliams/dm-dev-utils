## Purpose
To centralise some code snippets or ideas that may be useful for developers working on the Digital Marketplace.

## How To Use
* python-auto-venv:
  * Type 'venv make'/'venv3 make' to create blank python2/python3 virtual environments in the current directory.
  * When you cd around, you will automatically drop into/out of your venvs (searching in the current dir and its parents). 
  * Usage: Source the file at the end of your .bashrc/.bash_profile

* dm-secret-cli:
  * Type 'secret' on the CLI to toggle history on/off, e.g. to prevent storing tokens in bash history.
  * Usage: source the file at the end of your .bashrc/.bash_profile

* clean-db.py:
  * Drops and re-creates your local DM database from the exported data dump.
  * Usage: ./clean-db.py
