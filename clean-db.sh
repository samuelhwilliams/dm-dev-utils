#!/bin/bash

## WORK IN PROGRESS


CURR_DIR=$(pwd)
DB_FILE='/Users/samuelwilliams/Downloads/exportdata.sql'
API_REPO='/Users/samuelwilliams/git/digitalmarketplace-api'

if [ ! -f $DB_FILE ]; then
  /usr/bin/read -p 'Enter the absolute path to the database dump from Google Drive [eg ~/Downloads/exportdata.sql]: ' DB_FILE
fi

if [ ! -d $API_REPO ]; then
  /usr/bin/read -p 'Enter the absolute path to a current checkout of digitalmarketplace-api master branch [eg ~/git/digitalmarketplace-api]: ' API_REPO
fi

/usr/local/bin/psql postgres << EOF
drop database digitalmarketplace;
create database digitalmarketplace;
EOF

/usr/local/bin/psql digitalmarketplace < "${DB_FILE}"

/usr/local/bin/psql digitalmarketplace << EOF
DROP EVENT TRIGGER reassign_owned;
EOF

# Run API DB migrations
(cd $API_REPO && make run-migrations && cd $CURR_DIR)

for role in "buyer" "supplier" "admin" "admin-ccs-sourcing" "admin-ccs-category"; do
  /usr/local/bin/psql digitalmarketplace << EOF
    with users2 as (select id from users where role='$role' limit 1) update users set email_address='samuel.williams+$role@digital.cabinet-office.gov.uk', active=true, failed_login_count=0 from users2 where users.id = users2.id;
EOF
done
