#!/bin/bash

function get_current_git_branch_name() {
  echo $(git name-rev --name-only HEAD)
}

alias gco='git checkout'
alias gcob='gco -b'
alias gl='git log'
alias gd='git diff'
alias gr='git rebase'
alias gs='git status'
alias gc='git commit'
alias gcam='git commit -a --amend'
alias gpp='git pull --prune'
alias grm='git rebase master'
alias gpo='git push origin $(get_current_git_branch_name)'
alias grho='git reset --hard origin/$(get_current_git_branch_name)'
