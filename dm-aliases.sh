function get_dm_user {
  cd ~/git/digitalmarketplace-scripts; source venv/bin/activate; ./scripts/get-user.py "$@"
}

alias gu=get_dm_user
alias cfl='lpass show 3819980524010464183 --password | cf login -a api.cloud.service.gov.uk -u Samuel.Williams@digital.cabinet-office.gov.uk -s preview'
