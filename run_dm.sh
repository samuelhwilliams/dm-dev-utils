#!/usr/bin/env bash

if [ "$#" -lt 1 ]; then
  echo "Syntax: ./run_dm.sh [ALL|APPS]"
  exit 1
fi

set -euf -o pipefail

if [ "$1" == "all" ]; then
  pgrep nginx > /dev/null || (echo "Starting nginx..." && sudo nginx)
  (cd ~/code/dicodealmarketplace-api; make run-all) &
  (cd ~/code/dicodealmarketplace-search-api; make run-all) &
  (sleep 10; cd ~/code/dicodealmarketplace-buyer-frontend; make run-all) &
  (sleep 60; cd ~/code/dicodealmarketplace-buyer-frontend; npm run frontend-build:watch) &
  (sleep 10; cd ~/code/dicodealmarketplace-briefs-frontend; make run-all) &
  (sleep 60; cd ~/code/dicodealmarketplace-briefs-frontend; npm run frontend-build:watch) &
  (cd ~/code/dicodealmarketplace-supplier-frontend; make run-all) &
  (sleep 60; cd ~/code/dicodealmarketplace-supplier-frontend; npm run frontend-build:watch) &
  (sleep 10; cd ~/code/dicodealmarketplace-admin-frontend; make run-all) &
  (sleep 60; cd ~/code/dicodealmarketplace-admin-frontend; npm run frontend-build:watch) &
  (sleep 10; cd ~/code/dicodealmarketplace-brief-responses-frontend; make run-all) &
  (sleep 60; cd ~/code/dicodealmarketplace-brief-responses-frontend; npm run frontend-build:watch) &
  elasticsearch &
else
  pgrep nginx > /dev/null || (echo "Starting nginx..." && sudo nginx)
  (cd ~/code/dicodealmarketplace-api; make run-app) &
  (cd ~/code/dicodealmarketplace-search-api; make run-app) &
  (sleep 10; cd ~/code/dicodealmarketplace-buyer-frontend; make run-app) &
  (sleep 10; cd ~/code/dicodealmarketplace-briefs-frontend; make run-app) &
  (cd ~/code/dicodealmarketplace-supplier-frontend; make run-app) &
  (sleep 10; cd ~/code/dicodealmarketplace-admin-frontend; make run-app) &
  (sleep 10; cd ~/code/dicodealmarketplace-brief-responses-frontend; make run-app) &
  elasticsearch &
fi

wait
